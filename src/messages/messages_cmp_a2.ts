import { Component, Input, OnInit } from '@angular/core';
import { Message, Repository } from './repository';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'message-text',
  template: `
  <h1>Messages from A2</h1>
    <ul>
      <li *ngFor="let m of messages">
        {{m.id}} - <a href="/messages/{{folder}}/{{m.id}}">{{m.text}}</a>
      </li>
    </ul>
    <a href="/settings/pagesize">Change Page Size</a>
    <a href="/">Back</a>
  `
})
export class MessagesComponent implements OnInit {
    folder: string;
  messages: Message[];

  constructor( private repository: Repository, private activatedRoute : ActivatedRoute) {
    //this.folder = "inbox";
    
  }

  ngOnInit(){
    this.activatedRoute.params.subscribe(params => {
      this.folder = params['folder'];
      this.messages = this.repository.messagesFor(this.folder);
    });

  }
}


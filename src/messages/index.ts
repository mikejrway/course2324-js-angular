// This module is in the midst of transitiong from Angular 1 to Angular 2.
// We migrated MessageTextCmp to Angular2.
import * as angular from 'angular';
import {NgModule} from '@angular/core';
import {UpgradeModule, downgradeComponent} from '@angular/upgrade/static';

import {Repository} from './repository';
import {MessageTextCmp} from './message_text_cmp';

import {MessageCmp} from './message_cmp';
import { CommonModule } from '@angular/common';
import { RouterModule } from "@angular/router";
import { MessagesComponent } from './messages_cmp_a2';

export const MessagesModule = angular.module('MessagesModule', ['ngRoute']);


MessagesModule.component('message', MessageCmp);
MessagesModule.service('repository', Repository);
MessagesModule.config(($routeProvider) => {
  $routeProvider
//    .when('/messages/:folder',     {template : '<messages></messages>'})
    .when('/messages/:folder/:id', {template : '<message></message>'});
});

export function exportRepository(m: UpgradeModule): Repository {
  return m.$injector.get('repository');
}

@NgModule({
  // components migrated to Angular 2 should be registered here
  declarations: [MessageTextCmp, MessagesComponent],
  entryComponents: [MessageTextCmp],
imports : [ CommonModule,
RouterModule.forChild([
{ path: 'messages/:folder', component: MessagesComponent },
{ path: 'messages/:folder/:id', component: MessagesComponent },

])],
  providers: [
    {provide: Repository, useFactory: exportRepository, deps: [UpgradeModule]}
  ]
})
export class MessagesNgModule {}

// components migrated to angular 2 should be downgraded here
MessagesModule.directive('messageText', <any>downgradeComponent({
  component: MessageTextCmp,
  inputs: ['text']
}));

MessagesModule.directive('messages', <any>downgradeComponent({
  component: MessagesComponent,

}));